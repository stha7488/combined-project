# Circumference and Area of circle

import math

radius_str = input("Enter the radius of your circle")
radius_int = int(radius_str)

circ = 2*math.pi*radius_int
area = math.pi*(radius_int*2)

print("The circumference of circle = ", circ)
print("\n The area is: ",area)
print("\n hello world master")