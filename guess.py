# Demo of Random and loop

import random

number = random.randint(0,10)

print("Welcome to the guessing game. Lets Play game !!! \n")

guess_int = int(input("Guess a number between 0 and 10 "))

while 0 <= guess_int <=10:
    if guess_int > number:
        print("Guessed too high")
    elif guess_int<number:
        print("Guessed too low")
    else:
        print("You are correct")
        break
    guess_int = int(input("Try another number \n "))
else:
    print("You quit early the number was ", number)
