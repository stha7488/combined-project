Python 3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 19:29:22) [MSC v.1916 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> 1+4
5
>>> 22/5
4.4
>>> 22//5
4
>>> "Hi "+ " there"
'Hi  there'
>>> "Hi"*5
'HiHiHiHiHi'
>>> a_int = 27
>>> a_float = 3.79
>>> b_float = 3.79
>>> a_float== b_float
True
>>> a_float is b_float
False
>>> myList= [a_float,"hi",5]
>>> myList
[3.79, 'hi', 5]
>>> "hi".capitalize()
'Hi'
>>> Python 3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 19:29:22) [MSC v.1916 32 bit (Intel)] on win32
SyntaxError: invalid syntax
>>> 
>>> 
>>> a =2
>>> a = a+1
>>> a +=1
>>> a
4
>>> 3>2
True
>>> my_int = 7
>>> 0<=my_int <=10
True
>>> 0<=int and my_int<=10
Traceback (most recent call last):
  File "<pyshell#23>", line 1, in <module>
    0<=int and my_int<=10
TypeError: '<=' not supported between instances of 'int' and 'type'
>>> 0<=my_int and my_int<=10
True
>>> a,b = 2,3
>>> a
2
>>> b
3
>>> # swap
>>> a,b = b,a
>>> a
3
>>> b
2
>>> 
============== RESTART: C:/Users/Ashish/Desktop/Week_1/guess.py ==============
Welcome to the guessing game 

Guess a number between 0 and 1011
>>> 11
11
>>> 5
5
>>> 
============== RESTART: C:/Users/Ashish/Desktop/Week_1/guess.py ==============
Welcome to the guessing game 

Guess a number between 0 and 10 5
Guessed too high
Try another number 
 1
You are correct
>>> 
============== RESTART: C:/Users/Ashish/Desktop/Week_1/guess.py ==============
Welcome to the guessing game 

Guess a number between 0 and 10 5
Guessed too high
Try another number 
 -1
You quit early the number was  4
>>> 
>>> 
>>> 
>>> 
>>> # Example of for loop
>>> for i in "hi there"
SyntaxError: invalid syntax
>>> for i in "hi there":
	print (i)

	
h
i
 
t
h
e
r
e
>>> for i in range(0,10):
	print(i)

	
0
1
2
3
4
5
6
7
8
9
>>> 
>>> word = "Hello World"
>>> word[4]
'o'
>>> word[-1]
'd'
>>> # positive number : from start to end
>>> # neg num for end to start
>>> 
>>> word[3:7]
'lo W'
>>> word[3:-2]
'lo Wor'
>>> word[1:6:2]
'el '
>>> word[:]
'Hello World'
>>> word[1:3:2]
'e'
>>> word[3:]
'lo World'
>>> word[::-1]
'dlroW olleH'
>>> word[1:4:2]
'el'
>>> myword = "hi"
>>> myword2 = 'hi'
>>> name = 'Dr.\'s'
>>> name
"Dr.'s"
>>> name2 = "Dr.'s"
>>> name2
"Dr.'s"
>>> myword == myword2
True
>>> myword is myword2
True
>>> s = "fght \ t ash"
>>> s
'fght \\ t ash'
>>> s = "fght \t ash"
>>> s
'fght \t ash'
>>> 
============= RESTART: C:/Users/Ashish/Desktop/Week_1/Spacing.py =============
this is

            weird line
            fggh
this is 	 gjh 
 asklhdjkj
>>> len("word here")
9
>>> word ="Hello"
>>> len(word)
5
>>> a = "here"
>>> type(a)
<class 'str'>
>>> "a" < "b"
True
>>> "aaa"< "aab"
True
>>> "a" in "this word"
False
>>> "a" in Apple
Traceback (most recent call last):
  File "<pyshell#82>", line 1, in <module>
    "a" in Apple
NameError: name 'Apple' is not defined
>>> "a" in "Appl"e
SyntaxError: invalid syntax
>>> "a" in "Apple"
False
>>> my_str = "this word"
>>> my_str.find("s")
3
>>> my_str2 = "Hello"
>>> my_str2.find("l")
2
>>> my_str2.find("l",3)
3
>>> my_str2.find("l",my_str2.find("l")+1)
3
>>> a="hello"
>>> b=a
>>> b
'hello'
>>> b=a[-1]
>>> b
'o'
>>> b=a[::-1]
>>> b
'olleh'
>>> b=a[::-3]
>>> b
'oe'
>>> ord("a")
97
>>> chr(97)
'a'
>>> a= "hello"
>>> a[2:]
'llo'
>>> a[:2]
'he'
>>> a[::-1]
'olleh'
>>> a[-1::]
'o'
>>> 
